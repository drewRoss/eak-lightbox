export default Ember.Route.extend({
  actions: {
    open: function(name) {
      this.render(name, { into: 'index', outlet: 'modal', view: 'modal' });
    },
    open1: function() {
      this.send('open','modal1');
    },
    open2: function() {
      this.send('open','modal2');
    },
    open3: function() {
      this.send('open','modal3');
    },
 
    close: function() {
      this.disconnectOutlet({outlet: 'modal', parentView: 'index'});
    },
    
    save: function() {
      alert('actions work like normal!');
    }
  }

});
